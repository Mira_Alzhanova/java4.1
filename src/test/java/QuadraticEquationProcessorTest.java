
import QudraticEquation.QuadEq;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class QuadraticEquationProcessorTest {
    @Test
    void test(){
        Assertions.assertThrows(ArithmeticException.class,() -> {QuadraticEquationProcessor q = new QuadraticEquationProcessor(new QuadEq(1,2,3));q.getBiggerRoot();});
        Assertions.assertThrows(ArithmeticException.class,() -> {QuadraticEquationProcessor q = new QuadraticEquationProcessor(new QuadEq(0,0,3));q.getBiggerRoot();});
        QuadraticEquationProcessor q1 = new QuadraticEquationProcessor(new QuadEq(2,0,-4));
        Assertions.assertEquals(Math.sqrt(2d), q1.getBiggerRoot(),1e-10);

        QuadraticEquationProcessor q2 = new QuadraticEquationProcessor(new QuadEq(1,0,-4));
        Assertions.assertEquals(2d, q2.getBiggerRoot(),1e-10);

        QuadraticEquationProcessor q3 = new QuadraticEquationProcessor(new QuadEq(0,1,1));
        Assertions.assertEquals(1, q3.getBiggerRoot(),1e-10);
    }
}
