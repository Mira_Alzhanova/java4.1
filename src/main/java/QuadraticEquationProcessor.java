import QudraticEquation.QuadEq;

public class QuadraticEquationProcessor {
    private QuadEq qe;

    public QuadraticEquationProcessor(QuadEq qe) {
        this.qe = qe;
    }

    public double getBiggerRoot(){
        double[] answer = qe.solve();
        if(answer.length == 1){
            return answer[0];
        } else {
            return Math.max(answer[0], answer[1]);
        }
    }
}
